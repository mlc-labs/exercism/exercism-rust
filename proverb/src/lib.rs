pub fn build_proverb(list: Vec<&str>) -> String {
    let mut message = String::new();
    if list.len() == 0 {
        return message;
    }
    
    let last_item = list.len() - 1;
    let mut index = 0;
    while last_item > 0 && index < last_item  {
        let line = format!("For want of a {} the {} was lost.\n", list[index], list[index+1]);
        message.push_str(&line);
        index = index + 1;
    }
    message = format!("{}And all for the want of a {}.", message, list[0]);
    return String::from(message);
}
