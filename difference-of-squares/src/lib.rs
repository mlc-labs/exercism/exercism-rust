pub fn square_of_sum(n: usize) -> usize {
    let mut sum_values = 0;
    for i in 0..n {
        sum_values = sum_values + i + 1;
    }
    return sum_values.pow(2);
}

pub fn sum_of_squares(n: usize) -> usize {
    let mut squares_values = 0;
    for i in 0..n {
        squares_values = squares_values + (i + 1).pow(2);
    }
    return squares_values;
}

pub fn difference(n: usize) -> usize {
    return square_of_sum(n) - sum_of_squares(n)
}
